from socket import socket, AF_INET, SOCK_STREAM, SOL_SOCKET, SO_REUSEADDR
from threading import Thread


clients = set()


def client_thread(clientSocket, clientAddress):
    while True:
        message = clientSocket.recv(1024).decode("utf-8")
        print(clientAddress[0] + ":" + str(clientAddress[1]) + " says: " + message)
        for client in clients:
            if client is not clientSocket:
                client.send((clientAddress[0] + ":" + str(clientAddress[1]) + " says: " + message).encode("utf-8"))

        if not message:
            clients.remove(clientSocket)
            print(clientAddress[0] + ":" + str(clientAddress[1]) + " disconnected")
            break

    clientSocket.close()


host_socket = socket(AF_INET, SOCK_STREAM)
host_socket.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)

host_ip = "127.0.0.1"
port_num = 7500
host_socket.bind((host_ip, port_num))
host_socket.listen()
print("Waiting for connection...")


while True:
    client_socket, client_address = host_socket.accept()
    clients.add(client_socket)
    print("Connection established with: ", client_address[0] + ":" + str(client_address[1]))
    thread = Thread(target=client_thread, args=(client_socket, client_address, ))
    thread.start()
